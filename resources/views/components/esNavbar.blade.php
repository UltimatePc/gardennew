<nav class="navbar navbar-expand-lg navbar-light border bg-white py-0">
  <img src="/imgs/logos/the-garden-bar-logo-6.png" style="height: 75px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
  <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <ul class="navbar-nav text-center d-flex align-items-center mx-auto">
      <li class="nav-item{{ isset($active) && $active == 'Inicio' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('esIndex') }}">Inicio</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Bebidas' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('esDrinks') }}">Bebidas</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Comida' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('esFood') }}">Comida</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Noticias' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('esNews') }}">Noticias</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Galería' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('esGallery') }}">Galería</a>
      </li>
    </ul>
    <a class="nav-link text-center float-lg-right" href="@yield('langSwitch')"><button type="button" class="btn btn-primary">English</button></a>
  </div>
</nav>
