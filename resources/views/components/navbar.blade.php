<nav class="navbar navbar-expand-lg navbar-light border bg-white py-0">
  <img src="/imgs/logos/the-garden-bar-logo-6.png" style="height: 75px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
  <button class="navbar-toggler bg-light" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <ul class="navbar-nav text-center d-flex align-items-center mx-auto">
      <li class="nav-item{{ isset($active) && $active == 'Home' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('index') }}">Home</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Drinks' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('drinks') }}">Drinks Menu</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Food' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('food') }}">Food Menu</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'News' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('news') }}">News</a>
      </li>
      <li class="nav-item{{ isset($active) && $active == 'Gallery' ? ' active' : ''}}">
        <a class="nav-link" href="{{ route('gallery') }}">Gallery</a>
      </li>
    </ul>
    <a class="nav-link text-center float-lg-right" href="@yield('langSwitch')"><button type="button" class="btn btn-primary">Español</button></a>
  </div>
</nav>
