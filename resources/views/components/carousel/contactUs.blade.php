<div class="container">
  <div id="carouselcontactUs" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item d-flex justify-content-center active">
        <img data-src="/imgs/events/OCEAN2019/ocean-fest-2019-(136).JPG" class="lazy d-block w-50" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/events/FUNK2019/funk-fest-2019-(65).jpg" class="lazy d-block w-50" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/events/PDS2019/leatherback-pds-(109).JPG" class="lazy d-block w-50" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-8.jpg" class="lazy d-block w-50" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-4.jpg" class="lazy d-block w-50" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-hero-4.jpg" class="lazy d-block w-50" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
    </div>
  </div>

</div>
