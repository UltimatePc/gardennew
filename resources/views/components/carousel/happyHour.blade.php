<div class="container p-0">
  <div id="carouselHappyHour" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item d-flex justify-content-center active">
        <img data-src="/imgs/heros/the-garden-bar-drink-1.jpg" class="lazy d-block w-100" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-drink-3.jpg" class="lazy d-block w-100" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-drink-4.jpg" class="lazy d-block w-100" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-drink-5.jpg" class="lazy d-block w-100" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-drink-10.jpg" class="lazy d-block w-100" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
      <div class="carousel-item d-flex justify-content-center">
        <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy d-block w-100" style="max-height: 370px;" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
      </div>
    </div>
  </div>
</div>
