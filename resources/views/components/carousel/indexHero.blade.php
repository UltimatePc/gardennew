<div id="carouselHero" class="carousel slide carousel-fade" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img data-src="/imgs/heros/the-garden-bar-area-6.jpg" class="lazy d-block w-100" style="max-height:915px; min-height:300px" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/the-garden-bar-pool-4.jpg" class="lazy d-block w-100" style="max-height:915px; min-height:300px" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/the-garden-bar-hero-3.jpg" class="lazy d-block w-100" style="max-height:915px; min-height:300px" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/the-garden-bar-5.jpg" class="lazy d-block w-100" style="max-height:915px; min-height:300px" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/the-garden-bar-hero-1.jpg" class="lazy d-block w-100" style="max-height:915px; min-height:300px" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
    </div>
    <div class="carousel-item">
      <img data-src="/imgs/heros/the-garden-bar-front.jpg" class="lazy d-block w-100" style="max-height:915px; min-height:300px" alt="The Garden Bar, La Puerta Del Sol, Playas del Coco">
    </div>
  </div>
</div>
