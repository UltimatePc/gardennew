@extends('layouts.guest')

@section('title', 'Inicio')
@section('id', 'The Garden Bar')
@section('pageName', 'Inicio')
@section('langSwitch', '/')

@section('content')

@include('components.esNavbar', ['active' => 'Inicio'])

<div class="container-fluid">
  <div class="row no-gutters text-center">
    <div class="col-12 col-md-8 p-0">
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane border fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
          @include('components/carousel.indexHero')
        </div>

        <div class="tab-pane border fade" id="list-contact" role="tabpanel" aria-labelledby="list-contact-list">
          <div class="row my-3 mx-auto d-flex justify-content-center align-items-center">
          </div>
          <iframe data-src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15689.436293448593!2d-85.6939595!3d10.5510489!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x522a715854a2632e!2sThe%20Garden%20Bar!5e0!3m2!1sen!2sus!4v1572307000343!5m2!1sen!2sus" class="lazy" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

          <div class="row text-center no-gutters">
            <div class="col-12 col-lg-6 d-flex align-items-center px-0">
              <div class="container px-0 text-center mx-auto">
                <img class="lazy my-5" data-src="/imgs/logos/LPDSLogo2.png" style="height: 60px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                <p>
                  <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/phone.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - (+506) 2670-0195
                </p>
                <p>
                  <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/sms.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - (+506) 8542-5670
                </p>
                <p class="d-block d-lg-none text-primary">
                  <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.facebook.com/hotel.lapuertadelsol/">Haga clic aquí</a>
                </p>
                <p class="d-none d-lg-block text-primary">
                  <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.facebook.com/hotel.lapuertadelsol/">Facebook.com/hotel.lapuertadelsol/</a>
                </p>
                <p class="d-block d-lg-none text-primary">
                <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/filled-message.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="mailto:lapuertadelsolcostarica@gmail.com">Haga clic aquí</a>
                </p>
                <p class="d-none d-lg-block text-primary">
                <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/filled-message.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="mailto:lapuertadelsolcostarica@gmail.com">lapuertadelsolcostarica@gmail.com</a>
                </p>
                <p class="d-none d-lg-block text-primary">
                  <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.waze.com/ul?ll=10.55092230%2C-85.69387370&navigate=yes&zoom=16">Waze Orientación e indicaciones</a>
                </p>
                <p class="d-block d-lg-none text-primary">
                  <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.waze.com/ul?ll=10.55092230%2C-85.69387370&navigate=yes&zoom=16">Haga clic aquí</a>
                </p>
              </div>
            </div>

            <div class="col-12 col-lg-6 d-flex align-items-center px-0">
              <div class="container px-0 text-center mx-auto">
                <img class="lazy my-5" data-src="/imgs/logos/the-garden-bar-logo-6.png" style="height: 60px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                <p>
                  <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/phone.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - (+506) 2670-0195
                </p>
                <p>
                  <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/sms.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - (+506) 8542-5670
                </p>
                <p class="d-block d-lg-none text-primary">
                  <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.facebook.com/thegardenbar.cr/">Haga clic aquí</a>
                </p>
                <p class="d-none d-lg-block text-primary">
                  <img class="lazy" data-src="https://img.icons8.com/color/26/000000/facebook-new.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.facebook.com/thegardenbar.cr/">Facebook.com/thegardenbar.cr/</a>
                </p>
                <p class="d-block d-lg-none text-primary">
                  <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/instagram-new.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.instagram.com/the_garden_bar_cr/">Haga clic aquí</a>
                </p>
                <p class="d-none d-lg-block text-primary">
                  <img class="lazy" data-src="https://img.icons8.com/metro/26/000000/instagram-new.png" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.instagram.com/the_garden_bar_cr/">Instagram.com/the_garden_bar_cr/</a>
                </p>
                <p class="d-none d-lg-block text-primary">
                  <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.waze.com/ul?ll=10.55104890%2C-85.69395950&navigate=yes&zoom=16">Waze Orientación e indicaciones</a>
                </p>
                <p class="d-block d-lg-none text-primary">
                  <img class="lazy" data-src="/imgs/logos/waze-brands.svg" style="height: 25px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica"> - <a href="https://www.waze.com/ul?ll=10.55104890%2C-85.69395950&navigate=yes&zoom=16">Haga clic aquí</a>
                </p>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 mt-5 px-0">
              <p>Haga clic en el botón de descarga para descargar nuevos protocolos de seguridad</p>
              <a  download="Protocols.pdf" href="/docs/Protocols.pdf">
                <button type="button" class="btn btn-primary my-2">
                  Descarga
                </button>
              </a>
            </div>
          </div>
        </div>

        <div class="tab-pane border fade" id="list-location" role="tabpanel" aria-labelledby="list-location-list">
          <iframe class="lazy my-5" data-src="https://www.youtube.com/embed/t_wNzbvvEZQ?rel=0&autoplay=1&mute=1&showinfo=0" width="100%" height="400px" frameborder="0" allowfullscreen></iframe>
          <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
          <div class="container my-5">
            <p class="text-center">
              <b> The Garden Bar </b> en playas del Coco es un gran lugar para sentirse como en casa, estará rodeado de jardines tropicales y un área recientemente renovada para un momento de diversión y seguridad.
            </p>
            <p class="text-center">
              Únase a nosotros, ya que tenemos eventos y actividades diarias, así como happy hour de 2pm a 6pm, un nuevo menú de comida de nuestros chefs y camareros de calidad listos para servir la bebida fría de su elección.
            </p>
            <p class="text-center">
              Muchos turistas y locales eligen este lugar como su lugar diario, ¡ven a descubrir por qué! ¡Aquí en el Garden Bar!
            </p>
          </div>
        </div>

        <div class="tab-pane border fade" id="list-drinks" role="tabpanel" aria-labelledby="list-drinks-list">
          <div class="card-group">
            <div class="row">
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/heros/the-garden-bar-drink-1.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/heros/the-garden-bar-drink-3.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/heros/the-garden-bar-drink-4.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
            </div>
          </div>
          <p class="p-2 my-2">
            ¡Con la nueva cerveza artesanal Monkey Head y Numu recientemente agregada al bar, puede disfrutar de una excelente selección de whisky, ron, tequilas y nuestras famosas mezclas!<br>
            <a class="mx-2 text-center" href="{{ route('esDrinks') }}"><button type="button" class="btn btn-primary my-3">Menú de bebidas</button></a>
          </p>
          <div class="card-group">
            <div class="row">
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/heros/the-garden-bar-drink-5.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/heros/the-garden-bar-drink-10.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="tab-pane border fade" id="list-restaurant" role="tabpanel" aria-labelledby="list-restaurant-list">
          <div class="card-group">
            <div class="row">
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/food/garden-wings.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/food/garden-quesadilla.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/food/garden-bar-nachos.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
            </div>
          </div>
          <p class="p-2 my-2">
            Venga a disfrutar de sus platos favoritos y descubra nueva comida exótica del wok del jardín. Además, encuentre excelentes opciones de sándwiches para elegir.
            <a class="mx-2 text-center" href="{{ route('esFood') }}"><button type="button" class="btn btn-primary my-3">Menú de comida</button></a>
          </p>
          <div class="card-group">
            <div class="row">
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/food/garden-red-curry.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/food/garden-bar-cesar-salad.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
              <div class="col-4 p-0">
                <div class="card">
                  <img data-src="/imgs/food/garden-bar-banana-fritters.jpg" class="lazy card-img-top" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="col-12 col-md-4 p-0 middle border">
      <div class="col p-0">
        <img data-src="/imgs/logos/the-garden-bar-logo-4.png" class="lazy d-none d-md-block w-100 mt-5" alt="Playas del Coco Guanacaste, Hotel La puerta Del Sol, Costa Rica">
        <div class="list-group" id="list-tab" role="tablist">
          <a class="list-group-item list-group-item-success active mt-5" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Inicio</a>
          <a class="list-group-item list-group-item-success" id="list-location-list" data-toggle="list" href="#list-location" role="tab" aria-controls="location">Sobre The Garden Bar</a>
          <a class="list-group-item list-group-item-success" id="list-restaurant-list" data-toggle="list" href="#list-restaurant" role="tab" aria-controls="restaurant">Restaurante Gourment</a>
          <a class="list-group-item list-group-item-success" id="list-drinks-list" data-toggle="list" href="#list-drinks" role="tab" aria-controls="drinks">Selección de bebidas</a>
          <a class="list-group-item list-group-item-success" id="list-contact-list" data-toggle="list" href="#list-contact" role="tab" aria-controls="contact">Información de contacto y ubicación</a>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
