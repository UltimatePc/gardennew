@extends('layouts.guest')

@section('title', 'Comida')
@section('id', 'The Garden Bar')
@section('pageName', 'Comida')
@section('langSwitch', '/food')

@section('content')

@include('components.esNavbar', ['active' => 'Comida'])

<div class="container-fluid">
  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <div class="container text-center my-5">
    <h1>Appetizers</h1>
    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-wings.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Alitas de pollo</h4>
        </div>
        <div class="card-body">
          <p>Deliciosas alitas, teriyaki, barbacoa, nikkei, agridulce o picante <br> (5,500c - 8 alitas) o (10,000c - 16 alitas)</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-quesadilla.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Quesadillas</h4>
        </div>
        <div class="card-body">
          <p>Tortilla suave con cebollas y pimientos salteados con queso derretido y su elección de Pollo, Ternera o Vegetal. (4500c)</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-cheese-sticks.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Palitos de queso (5 piezas)</h4>
        </div>
        <div class="card-body">
          <p>Palitos de mozzarella fritos servidos con salsa marinara</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-yuca.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Yuca Crujiente (6Pcs)</h4>
        </div>
        <div class="card-body">
          <p>Mezcla de raíces de casava refritas con ajo y culantro, acompañamiento de lima y frijoles negros refritos</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-nachos.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Nachos</h4>
        </div>
        <div class="card-body">
          <p>Chips de tortilla, pico de gallo, guacamole, salsa de queso crema agria, frijoles negros refritos y pollo</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-red-curry.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Chicken Red Curry</h4>
        </div>
        <div class="card-body">
          <p>Viene con vegetales tropicales combinados con leche de coco. Acompañado de arroz japonés y ensalada verde</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-chicken-parmesan.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Pollo Parmesano</h4>
        </div>
        <div class="card-body">
          <p>Pasta cubierta con nuestro pollo parmesano con salsa boloñesa y una guarnición de pan de ajo</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-fish-tacos.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Tacos de Pescado</h4>
        </div>
        <div class="card-body">
          <p>Lubina a la parrilla o frita servida con tortillas de maíz. Cubierto con repollo rallado, pico de gallo con nuestra salsa tártara y jalapeños</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-chicken-teriyaki.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Pollo Teriyaki</h4>
        </div>
        <div class="card-body">
          <p>Pollo con salsa teriyaki glaseada con cebolla acompañado de arroz japonés y vegetales mixtos</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-cesar-salad.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Ensalada Cesar De Pollo</h4>
        </div>
        <div class="card-body">
          <p>El giro del Garden Bar a una tradicional ensalada Cesar de pollo</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-banana-fritters.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Banana Frita</h4>
        </div>
        <div class="card-body">
          <p>¡Ven a probar este delicioso desierto! solo en el Garden Bar</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-fried-ice.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Tempura de Helado Frito</h4>
        </div>
        <div class="card-body">
          <p>¡Un Delicioso Postre "La Puerta del Sol"!</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-volcano-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Rollo Volcán</h4>
        </div>
        <div class="card-body">
          <p>Futomaki de California, tempura frita con atún, salmón, marlín y salsa gratinada masago. Salsa de anguila encima</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-dragon-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Rollo de Dragón</h4>
        </div>
        <div class="card-body">
          <p>Anguila ahumada, camarones tempura crujientes, cubiertas de pepino con aguacate y anguila ahumada, salsa de anguila encima</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-california-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Rollo California</h4>
        </div>
        <div class="card-body">
          <p>Kani-kama, aguacate, pepino y semillas de sésamo</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-spicy-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Rollito de Atún Picante</h4>
        </div>
        <div class="card-body">
          <p>Atún recién cortado, aguacate, cebollino, salsa sriracha con ajonjolí</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-tuna-sashimi.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Sashimi de Atún</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-salmon-sashimi.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Sashimi de Salmón</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-edamame.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Edamame</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-saki.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Saki</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
    </div>

  </div>

  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <div class="container text-center my-5">
    <h1>Sandwiches</h1>
    <div class="card-group text-center">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-cheese-slider.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Hamburguesa con Queso (2 piezas)</h4>
        </div>
        <div class="card-body">
          <p>Black Angus jugoso y cebolla caramelizada, queso cheddar cremoso y pepinillos en rodajas</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-parm-slider.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Pollo a la Parmesana (2 piezas)</h4>
        </div>
        <div class="card-body">
          <p>Pechuga condimentada en rodajas, queso mozzarella, queso parmesano con salsa de tomate marinara y pepinillos en rodajas</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-blt-sandwhich.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Sándwich BLT</h4>
        </div>
        <div class="card-body">
          <p>Tocino crujiente, lechuga verde, rodajas de tomate en pan tostado blanco con mayonesa (los deslizadores vienen con una guarnición de papas fritas)</p>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
