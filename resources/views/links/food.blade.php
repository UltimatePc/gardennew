@extends('layouts.guest')

@section('title', 'Food Menu')
@section('id', 'The Garden Bar')
@section('pageName', 'Food Menu')
@section('langSwitch', '/comida')

@section('content')

@include('components.navbar', ['active' => 'Food'])

<div class="container-fluid">
  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <div class="container text-center my-5">
    <h1>Appetizers</h1>
    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-wings.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Chicken Wings</h4>
        </div>
        <div class="card-body">
          <p>Delicious wings, Teriyaki, BBQ, Nikkei, Sweet & Sour or Spicy <br> (5,500c - 8 wings) or (10,000c - 16 wings)</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-quesadilla.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Quesadillas</h4>
        </div>
        <div class="card-body">
          <p>Soft tortilla with sautéed onions and peppers with melted cheese and your choice of Chicken, Beef or Veggie. (4,500c)</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-cheese-sticks.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Cheese Sticks (5Pcs)</h4>
        </div>
        <div class="card-body">
          <p>Deep-Fried Mozzarella Sticks Serve With Marinara Sauce</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-yuca.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Crispy Yuca (6Pcs)</h4>
        </div>
        <div class="card-body">
          <p>Refried Casava Roots  Toss With Garlic & Culantro, Side With Lime & Refried Black Beans</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-nachos.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Nachos</h4>
        </div>
        <div class="card-body">
          <p>Tortilla Chips, Pico de Gallo, Guacamole, Sour Cream Cheese Sauce, Refried Black Beans & Chicken</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-red-curry.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Chicken Red Curry</h4>
        </div>
        <div class="card-body">
          <p>Comes with tropical vegetables combined with coconut milk. Accompanied with Japanese rice and green salad</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-chicken-parmesan.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Chicken Parmesan</h4>
        </div>
        <div class="card-body">
          <p>Pasta topped with our Chicken Parmesan with a Bolognese sauce with a side of Garlic Bread</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-fish-tacos.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Fish Tacos</h4>
        </div>
        <div class="card-body">
          <p>Grill or Deep-fried Sea Bass served con Corn Tortillas. Topped with shredded Cabbage, Pico de gallo with our Tartar, and Jalapenos sauces</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-chicken-teriyaki.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Chicken Teriyaki</h4>
        </div>
        <div class="card-body">
          <p>Chicken with an Onion glazed teriyaki sauce accompanied with Japanese rice and mixed vegetables</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-cesar-salad.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Chicken Cesar Salad</h4>
        </div>
        <div class="card-body">
          <p>The Garden Bar's twist to a traditional Chicken Cesar Salad</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-banana-fritters.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Banana Fritters</h4>
        </div>
        <div class="card-body">
          <p>Come try this delicious looking dessert! only at the Garden Bar</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-fried-ice.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Fried Ice Cream Tempura</h4>
        </div>
        <div class="card-body">
          <p>A Delicious "La Puerta del Sol" Dessert!</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-volcano-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Volcano Roll</h4>
        </div>
        <div class="card-body">
          <p>California futomaki, deep-fried tempura with tuna, salmon, marlin, and masago gratin sauce. Eel sauce on top</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-dragon-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Dragon Roll</h4>
        </div>
        <div class="card-body">
          <p>Smoked Eel, Crispy tempura shrimp, cucumber covers with avocado, and smoked eel, Eel sauce on top</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-california-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">California Roll</h4>
        </div>
        <div class="card-body">
          <p>Kani-kama, avocado, cucumber, and sesame seeds</p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-spicy-roll.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Spicy Tuna Roll</h4>
        </div>
        <div class="card-body">
          <p>Freshly cut tuna, avocado, chives, srirachha sauce with sesame seeds</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-tuna-sashimi.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Tuna Sashimi</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-salmon-sashimi.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Salmon Sashimi</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-edamame.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Edamame</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-saki.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Saki</h4>
        </div>
        <div class="card-body">
          <p></p>
        </div>
      </div>
    </div>

  </div>

  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <div class="container text-center my-5">
    <h1>Sandwiches</h1>
    <div class="card-group text-center">
      <div class="card">
        <img data-src="/imgs/food/garden-bar-cheese-slider.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Cheese Burger Slider (2Pcs)</h4>
        </div>
        <div class="card-body">
          <p>Juicy Black Angus & Caramelized Onion, Creamy Cheddar Cheese & Sliced Pickles</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-bar-parm-slider.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">Chicken Parmesan Sliders (2Pcs)</h4>
        </div>
        <div class="card-body">
          <p>Sliced Seasoned Breast, Mozzarella Cheese, Parmesan Cheese with Marinara Tomato Sauce & Sliced Pickles</p>
        </div>
      </div>
      <div class="card">
        <img data-src="/imgs/food/garden-blt-sandwhich.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
        <div class="card-title">
          <h4 class="font-italic text-primary mt-2">BLT Sandwich</h4>
        </div>
        <div class="card-body">
          <p>Crispy Bacon, Green Lettuce, Tomato Slices in White Toast Bread with Mayonnaise (Sliders Come With a Side of French Fries)</p>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
