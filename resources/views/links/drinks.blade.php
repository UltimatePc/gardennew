@extends('layouts.guest')

@section('title', 'Drinks')
@section('id', 'The Garden Bar')
@section('pageName', 'Drinks')
@section('langSwitch', '/bebidas')

@section('content')

@include('components.navbar', ['active' => 'Drinks'])

<div class="container-fluid text-center">
  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <h1 class="mt-2">Happy Hour everyday!</h1>
  <p>Join us between 2pm - 6pm</p>

  <div class="d-none d-md-block">
    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-1.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-3.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-4.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-5.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-10.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
    </div>
  </div>

  <div class="d-block d-md-none">
    @include('components/carousel.happyHour')
  </div>

  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <div class="container text-center my-5">
    <h1>Specialty Shots</h1>
    <p>(All shots below are between 1,500c - 3,500c)</p>
    <div class="row">
      <div class="col-12 col-md-3">
        <ul>
          <li>Cinnamon Toast Crunch</li>
          <li>Chiliguaro</li>
          <li>Miguelito</li>
          <li>Mini Beer</li>
        </ul>
      </div>
      <div class="col-12 col-md-3">
        <ul>
          <li>Green Tea Shot</li>
          <li>Pickle Back</li>
          <li>Kamikaze</li>
          <li>Blow Job</li>
        </ul>
      </div>
      <div class="col-12 col-md-3">
        <ul>
          <li>Beauty & the beast</li>
          <li>Iron Car Bomb</li>
          <li>Bazooka Joe</li>
          <li>Jagerbomb</li>
        </ul>
      </div>
      <div class="col-12 col-md-3">
        <ul>
          <li>Russian Roulette</li>
          <li>Vegas Bomb</li>
          <li>Super Man</li>
          <li>B-52</li>
        </ul>
      </div>
    </div>
  </div>

</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Signature Drinks</h1>
  <p>(All drinks below are 3,900c)</p>
  <div class="card-group">
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-5.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">EL SOL MULE</h4>
      </div>
      <div class="card-body">
        <p>Starting with Vodka added in with Pineapple juice and Rosemary, mix with some delicious Ginger Beer</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-10.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">PALO VERDE</h4>
      </div>
      <div class="card-body">
        <p>Gin based mixed with some Cucumber added with Basil and Cinnamon. Blended with lemon juice</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-4.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">DRUNK PIRATE</h4>
      </div>
      <div class="card-body">
        <p>Delicious Caribbean-style mix, Rum, and Borbon, ginger, cinnamon, coconut, honey, and lemon strings</p>
      </div>
    </div>
  </div>
  <div class="card-group text-center">
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-1.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">CHICHA PIÑA</h4>
      </div>
      <div class="card-body">
        <p>Costa Rican fermented Pineapple tequila blended Jalapeños to add quite the kick to this piña</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-3.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">COCO RAYADO</h4>
      </div>
      <div class="card-body">
        <p>This mix includes traditional Malibu with Vodka, a touch of honey, and Maracuya. Blended with Ginger and Soda</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">RUBY SUNSET MARGARITA</h4>
      </div>
      <div class="card-body">
        <p>This tequila will remind you of sunset when you taste the orange juice with syrup made from beet and a perfect amount of lemon juice</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">RUMBLE IN THE JUNGLE</h4>
      </div>
      <div class="card-body">
        <p>Gin based drink with a delicious syrup made from beet xzwith a gentle touch of lemon juice and topped with tonic</p>
      </div>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Premium Cocktails</h1>
  <p>(All drinks below are 6,000c)</p>
  <div class="row">
    <div class="col-12 col-md-6">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drinks-1.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Premium Margarita
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drink-6.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Strawberry Costa Rican Paloma
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-6">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drink-8.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Blue Troubles in the Garden
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drink-hero.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Rum Fashion
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Classic Cocktails</h1>
  <p>(All drinks below are 3,500c)</p>
  <div class="row">
    <div class="col-12 col-md-4">
      <ul>
        <li>Gin Sour Mint</li>
        <li>Moscow Mule</li>
        <li>Negroni</li>
        <li>Mai Tai</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <ul>
        <li>Long Island Iced Tea</li>
        <li>Piña Colada</li>
        <li>Margarita</li>
        <li>Caiprina</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <ul>
        <li>Bloody Mary</li>
        <li>French 75</li>
        <li>Martini</li>
        <li>Mojito</li>
      </ul>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Wine's</h1>
  <p>(All drinks below are 3,500c)</p>
  <div class="row">
    <div class="col-12 col-md-6">
      <h5><b>Red Wine</b></h5>
      <ul>
        <li>Cabernet - Sauvignon</li>
        <li>Pinot Noir</li>
        <li>Merlot</li>
        <li>Malbec</li>
      </ul>
    </div>
    <div class="col-12 col-md-6">
      <ul>
        <h5><b>White Wine</b></h5>
        <li>Sauvignon - Blanc</li>
        <li>Pinot Grigio</li>
        <li>Chardonnay</li>
      </ul>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 250px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 200px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Monkey Head Brewery Taproom</h1>
  <p>(All drinks below are between 3,000c - 3,100c)</p>
  <div class="row">
    <div class="col-12 col-md-6">
      <ul>
        <li>Space Monkey IPA</li>
        <li>Golden Ale</li>
      </ul>
    </div>
    <div class="col-12 col-md-6">
      <ul>
        <li>Sour Monkey</li>
        <li>Cas Cider</li>
      </ul>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 250px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 200px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>The Growler Program</h1>
  <p>Buy a Growler</p>
  <p>8,000c</p>
  <p>Refill your Growler with Monkey Head Product</p>
  <p>12,000c</p>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5 p-0">
  <h1>National Beers</h1>
  <p>(All drinks below are 1,800c)</p>
  <div class="row">
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/imperial-set.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Imperial <br><br> Imperial Light <br><br> Imperial Silver
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/pilsen.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Pilsen
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>

  <h1>Specialty Beers</h1>
  <p>(All drinks below are between 2,800c - 3,100c)</p>
  <div class="row">
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/bavaria-dark.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Bavaria Dark
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/bavaria.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Bavaria
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/heineken.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Hieneken
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/stella-artois.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Stella Artois
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/monkey-head.jpeg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Monkey Head
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/bavaria-light.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Bavaria Light
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/moosehead.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Moosehead
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/corona.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Corona
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/guinness.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Guinness
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/numu.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Numu
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5 p-0">
  <h1>Extra Drinks</h1>
  <p>(All drinks below are 2,000c)</p>
  <div class="row">
    <div class="col-6 p-0">
      <h4>Milkshakes</h4>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/chocolate-milkshake.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Chocolate
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/vanilla-milkshake.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Vanilla
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/strawberry-milkshake.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Strawberry
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6 p-0">
      <h4>Smoothies</h4>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/watermelon-smoothie.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Watermelon
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/lemonade-smoothie.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Lemonade
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/strawberry-smoothie.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Strawberry
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
