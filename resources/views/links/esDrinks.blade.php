@extends('layouts.guest')

@section('title', 'Bebidas')
@section('id', 'The Garden Bar')
@section('pageName', 'Bebidas')
@section('langSwitch', '/drinks')

@section('content')

@include('components.esNavbar', ['active' => 'Bebidas'])

<div class="container-fluid text-center">
  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <h1 class="mt-2">Happy Hour todos los días!</h1>
  <p>Ven con nosotros entre las 2pm y las 6pm</p>

  <div class="d-none d-md-block">
    <div class="card-group">
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-1.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-3.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-4.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-5.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-10.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
      <div class="card">
        <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      </div>
    </div>
  </div>

  <div class="d-block d-md-none">
    @include('components/carousel.happyHour')
  </div>

  <img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
  <div class="container text-center my-5">
    <h1>Shots Especiales</h1>
    <p>(Todas los shots a continuación estan de 1,500c - 3,500c)</p>
    <div class="row">
      <div class="col-12 col-md-3">
        <ul>
          <li>Cinnamon Toast Crunch</li>
          <li>Chiliguaro</li>
          <li>Miguelito</li>
          <li>Mini Beer</li>
        </ul>
      </div>
      <div class="col-12 col-md-3">
        <ul>
          <li>Green Tea Shot</li>
          <li>Pickle Back</li>
          <li>Kamikaze</li>
          <li>Blow Job</li>
        </ul>
      </div>
      <div class="col-12 col-md-3">
        <ul>
          <li>Beauty & the beast</li>
          <li>Iron Car Bomb</li>
          <li>Bazooka Joe</li>
          <li>Jagerbomb</li>
        </ul>
      </div>
      <div class="col-12 col-md-3">
        <ul>
          <li>Russian Roulette</li>
          <li>Vegas Bomb</li>
          <li>Super Man</li>
          <li>B-52</li>
        </ul>
      </div>
    </div>
  </div>

</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Bebidas Exclusivas</h1>
  <p>(Todas las bebidas a continuación son 3,900c)</p>
  <div class="card-group">
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-5.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">EL SOL MULE</h4>
      </div>
      <div class="card-body">
        <p>Comenzando con vodka, jugo de piña y romero, mezclado con una deliciosa cerveza de jengibre</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-10.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">PALO VERDE</h4>
      </div>
      <div class="card-body">
        <p>Una base de ginebra mezclada con un poco de pepino, Albahaca y Canela. Mezclado con jugo de limón</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-4.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">PIRATA BORRACHO</h4>
      </div>
      <div class="card-body">
        <p>Deliciosa mezcla de estilo caribeño, ron y Borbon, jengibre, canela, coco, miel y ralladura de limón</p>
      </div>
    </div>
  </div>
  <div class="card-group text-center">
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-1.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">CHICHA PIÑA</h4>
      </div>
      <div class="card-body">
        <p>Bebida costarricense de piña fermentada mezclada con jalapeños para darle un toque especial a esta piña</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-3.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">COCO RAYADO</h4>
      </div>
      <div class="card-body">
        <p>Esta mezcla incluye el tradicional Malibu con vodka, un toque de miel y maracuya. Mezclado con jengibre y soda</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">MARGARITA RUBY SUNSET</h4>
      </div>
      <div class="card-body">
        <p>Este tequila te recordará una puesta de sol cuando pruebes el jugo de naranja con jarabe de remolacha y una cantidad perfecta de jugo de limón</p>
      </div>
    </div>
    <div class="card">
      <img data-src="/imgs/heros/the-garden-bar-drink-9.jpg" class="lazy card-img-top" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
      <div class="card-title">
        <h4 class="font-italic text-primary mt-2">RUMBLE IN THE JUNGLE</h4>
      </div>
      <div class="card-body">
        <p>Bebida a base de ginebra con un delicioso jarabe de remolacha con un toque suave de jugo de limón y cubierto con tónico</p>
      </div>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Cócteles Premium</h1>
  <p>(Todas las bebidas a continuación son 6,000c)</p>
  <div class="row">
    <div class="col-12 col-md-6">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drinks-1.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Margarita Premium
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drink-6.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Fresa Paloma Costarricense
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-6">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drink-8.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Problemas azules en el jardín
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/the-garden-bar-drink-hero.jpg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Moda del Rum
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Cócteles Clásicos</h1>
  <p>(Todas las bebidas a continuación son 3,500c)</p>
  <div class="row">
    <div class="col-12 col-md-4">
      <ul>
        <li>Gin Sour Mint</li>
        <li>Moscow Mule</li>
        <li>Negroni</li>
        <li>Mai Tai</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <ul>
        <li>Long Island Iced Tea</li>
        <li>Piña Colada</li>
        <li>Margarita</li>
        <li>Caiprina</li>
      </ul>
    </div>
    <div class="col-12 col-md-4">
      <ul>
        <li>Bloody Mary</li>
        <li>French 75</li>
        <li>Martini</li>
        <li>Mojito</li>
      </ul>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Vino</h1>
  <p>(Todas las bebidas a continuación son 3,500c)</p>
  <div class="row">
    <div class="col-12 col-md-6">
      <h5><b>Vino Tinto</b></h5>
      <ul>
        <li>Cabernet - Sauvignon</li>
        <li>Pinot Noir</li>
        <li>Merlot</li>
        <li>Malbec</li>
      </ul>
    </div>
    <div class="col-12 col-md-6">
      <ul>
        <h5><b>Vino blanco</b></h5>
        <li>Sauvignon - Blanc</li>
        <li>Pinot Grigio</li>
        <li>Chardonnay</li>
      </ul>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 250px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 200px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Taproom Cervecería Monkey Head</h1>
  <p>(Todas las bebidas a continuación son between 3,000c - 3,100c)</p>
  <div class="row">
    <div class="col-12 col-md-6">
      <ul>
        <li>Space Monkey IPA</li>
        <li>Golden Ale</li>
      </ul>
    </div>
    <div class="col-12 col-md-6">
      <ul>
        <li>Sour Monkey</li>
        <li>Cas Cider</li>
      </ul>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 250px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/heros/monkey-head.jpeg" style="max-height: 200px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5">
  <h1>Programa Growler</h1>
  <p>Compre un Growler</p>
  <p>8,000c</p>
  <p>Rellene su Growler con el producto Monkey Head</p>
  <p>12,000c</p>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5 p-0">
  <h1>Cervezas Nacionales</h1>
  <p>(Todas las bebidas a continuación son 1,800c)</p>
  <div class="row">
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/imperial-set.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Imperial <br><br> Imperial Light <br><br> Imperial Silver
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/pilsen.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Pilsen
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>

  <h1>Cervezas Especializadas</h1>
  <p>(Todas las bebidas a continuación son between 2,800c - 3,100c)</p>
  <div class="row">
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/bavaria-dark.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Bavaria Dark
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/bavaria.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Bavaria
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/heineken.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Hieneken
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/stella-artois.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Stella Artois
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/monkey-head.jpeg" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Monkey Head
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6 p-0">
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/bavaria-light.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Bavaria Light
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/moosehead.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Moosehead
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/corona.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Corona
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/guinness.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Guinness
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/numu.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Numu
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<img class="lazy d-none d-md-block mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 100px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<img class="lazy d-block d-md-none mx-auto mt-5" data-src="/imgs/logos/the-garden-bar-logo-5.png" style="max-height: 50px;" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
<div class="container text-center my-5 p-0">
  <h1>Bebidas Extras</h1>
  <p>(Todas las bebidas a continuación son 2,000c)</p>
  <div class="row">
    <div class="col-6 p-0">
      <h4>Batidos</h4>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/chocolate-milkshake.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Chocolate
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/vanilla-milkshake.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Vainilla
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/strawberry-milkshake.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Fresa
            </h4>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6 p-0">
      <h4>Batidos</h4>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/watermelon-smoothie.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Sandía
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/lemonade-smoothie.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Limonada
            </h4>
          </div>
        </div>
      </div>
      <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <img data-src="/imgs/heros/strawberry-smoothie.png" class="lazy card-img" alt="The Garden Bar, Hotel La puerta Del Sol, Playas del Coco Guanacaste, Costa Rica">
          </div>
          <div class="col-md-8 middle">
            <h4 class="font-italic text-primary">
              Fresa
            </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
