<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestPagesController extends Controller
{
  public function index()
  {
      return view('links.index');
  }

  public function esIndex()
  {
      return view('links.esIndex');
  }

  public function drinks()
  {
      return view('links.drinks');
  }

  public function esDrinks()
  {
      return view('links.esDrinks');
  }

  public function food()
  {
      return view('links.food');
  }

  public function esFood()
  {
      return view('links.esFood');
  }

  public function news()
  {
      return view('links.news');
  }

  public function esNews()
  {
      return view('links.esNews');
  }

  public function gallery()
  {
      return view('links.gallery');
  }

  public function esGallery()
  {
      return view('links.esGallery');
  }

  public function papagayo2019()
  {
      return view('links/gallery.papagayo2019');
  }

  public function esPapagayo2019()
  {
      return view('links/gallery.esPapagayo2019');
  }

  public function oceanfest2019()
  {
      return view('links/gallery.oceanfest2019');
  }

  public function esOceanfest2019()
  {
      return view('links/gallery.esOceanfest2019');
  }

  public function leatherback2019()
  {
      return view('links/gallery.leatherback2019');
  }

  public function esLeatherback2019()
  {
      return view('links/gallery.esLeatherback2019');
  }

  public function papagayo2018()
  {
      return view('links/gallery.papagayo2018');
  }

  public function esPapagayo2018()
  {
      return view('links/gallery.esPapagayo2018');
  }

  public function carnival2020()
  {
      return view('links/gallery.carnival2020');
  }

  public function esCarnival2020()
  {
      return view('links/gallery.esCarnival2020');
  }

  public function bluesfest2020()
  {
      return view('links/gallery.bluesfest2020');
  }

  public function esbluesfest2020()
  {
      return view('links/gallery.esbluesfest2020');
  }
}
