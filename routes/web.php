<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GuestPagesController@index')->name('index');
Route::get('/news', 'GuestPagesController@news')->name('news');
Route::get('/food', 'GuestPagesController@food')->name('food');
Route::get('/drinks', 'GuestPagesController@drinks')->name('drinks');
Route::get('/gallery', 'GuestPagesController@gallery')->name('gallery');
Route::get('/gallery/carnival2020', 'GuestPagesController@carnival2020')->name('carnival2020');
Route::get('/gallery/papagayo2018', 'GuestPagesController@papagayo2018')->name('papagayo2018');
Route::get('/gallery/papagayo2019', 'GuestPagesController@papagayo2019')->name('papagayo2019');
Route::get('/gallery/oceanfest2019', 'GuestPagesController@oceanfest2019')->name('oceanfest2019');
Route::get('/gallery/bluesfest2020', 'GuestPagesController@bluesfest2020')->name('bluesfest2020');
Route::get('/gallery/leatherback2019', 'GuestPagesController@leatherback2019')->name('leatherback2019');

Route::get('/es', 'GuestPagesController@esIndex')->name('esIndex');
Route::get('/comida', 'GuestPagesController@esFood')->name('esFood');
Route::get('/noticias', 'GuestPagesController@esNews')->name('esNews');
Route::get('/bebidas', 'GuestPagesController@esDrinks')->name('esDrinks');
Route::get('/galería', 'GuestPagesController@esGallery')->name('esGallery');
Route::get('/galería/papagayo2018', 'GuestPagesController@esPapagayo2018')->name('esPapagayo2018');
Route::get('/galería/papagayo2019', 'GuestPagesController@esPapagayo2019')->name('esPapagayo2019');
Route::get('/gallery/esCarnival2020', 'GuestPagesController@esCarnival2020')->name('esCarnival2020');
Route::get('/galería/oceanfest2019', 'GuestPagesController@esOceanfest2019')->name('esOceanfest2019');
Route::get('/gallery/esbluesfest2020', 'GuestPagesController@esbluesfest2020')->name('esbluesfest2020');
Route::get('/galería/leatherback2019', 'GuestPagesController@esLeatherback2019')->name('esLeatherback2019');
